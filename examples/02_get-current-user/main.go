package main

import (
	"context"
	"log"
	"os"

	coinbase "gitlab.com/hmlkao/coinbase-go"
	"gitlab.com/hmlkao/coinbase-go/client"
)

func main() {
	config := coinbase.NewConfiguration()

	// 1. Create HTTP client which add required headers
	HTTPclient := client.GetClient(os.Getenv("COINBASE_KEY"), os.Getenv("COINBASE_SECRET"))
	config.HTTPClient = &HTTPclient

	// 2. Create Coinbase client
	client := coinbase.NewAPIClient(config)

	// 3. Create context
	ctx := context.Background()

	// 4. Use Coinbase client to get current user ID
	currentUser, _, err := client.UsersApi.CurrentUser(ctx)
	if err != nil {
		log.Fatalf("fail to get current user: %s", err)
	}

	userID, ok := currentUser.Data["id"].(string)
	if ok == false {
		log.Fatal("no ID given!")
	}

	// Yes, we will get the same information, this is only for example purpose
	user, _, err := client.UsersApi.User(ctx, userID)
	if err != nil {
		log.Fatal(err)
	}

	// 5. Show current user name
	log.Printf("name is: %#v", user.Data["name"])
}
