Get current server time
=======================
This example doesn't require Authentication to Coinbase so it work as is.

Just run the code
```
go run main.go
```

It print Unix timestamp received from Coinbase server.
