package main

import (
	"context"
	"log"

	coinbase "gitlab.com/hmlkao/coinbase-go"
)

func main() {
	config := coinbase.NewConfiguration()

	// 1. Create Coinbase client
	client := coinbase.NewAPIClient(config)

	// 2. Create context
	ctx := context.Background()

	// 3. Use Coinbase client to get current time
	time, _, err := client.TimeApi.Time(ctx)
	if err != nil {
		log.Fatalf("fail to get server time: %s", err)
	}

	// 4. Show timestamp
	log.Printf("server timestamp: %v", time.Data.Epoch)
}
