package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	coinbase "gitlab.com/hmlkao/coinbase-go"
	"gitlab.com/hmlkao/coinbase-go/client"
)

func main() {
	// 1. Load Coinbase configuration
	config := coinbase.NewConfiguration()

	// 2. Create HTTP client which add required headers
	HTTPclient := client.GetClient(os.Getenv("COINBASE_KEY"), os.Getenv("COINBASE_SECRET"))
	config.HTTPClient = &HTTPclient

	// 3. Create Coinbase client
	client := coinbase.NewAPIClient(config)

	// 4. Create context
	ctx := context.Background()

	// 5. Use Coinbase client to get account
	accounts, res, err := client.AccountsApi.Accounts(ctx)
	if err != nil {
		resBody, _ := ioutil.ReadAll(res.Body)
		if string(resBody) != "" {
			log.Printf("response body: %s", string(resBody))
		}
		log.Fatalf("failed to get accounts: %s", err)
	}

	fmt.Printf("\n\tOnly accounts with some amount are shown\n\n")

	// 6. Show accounts
	for _, account := range accounts.Data {
		amount, err := strconv.ParseFloat(account.Balance.Amount, 32)
		if err != nil {
			continue
		}
		if amount == 0 {
			continue
		}
		log.Printf("%s: %s %s", account.Name, account.Balance.Amount, account.Balance.Currency)
	}
}
