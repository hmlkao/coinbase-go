Get accounts balance
====================
You MUST setup Coinbase APIkey and APIsecret to your env

1. Log in to your [Coinbase account](https://coinbase.com)
1. Go to Profile Settings > API Access
1. Add new API Key
1. Setup values to Bash variables
   ```
   export COINBASE_KEY=<your-api-key>
   export COINBASE_SECRET=<your-api-secret>
   ```
1. Build and run example
   ```
   go run main.go
   ```
