module update-current-user

go 1.15

require (
	github.com/antihax/optional v1.0.0
	gitlab.com/hmlkao/coinbase-go v0.0.0-20201029104036-3beec838c1de
)
