package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/antihax/optional"
	coinbase "gitlab.com/hmlkao/coinbase-go"
	"gitlab.com/hmlkao/coinbase-go/client"
)

func main() {
	newName := "John Doe"

	// 1. Load Coinbase configuration
	config := coinbase.NewConfiguration()

	// 2. Create HTTP client which add required headers
	HTTPclient := client.GetClient(os.Getenv("COINBASE_KEY"), os.Getenv("COINBASE_SECRET"))
	config.HTTPClient = &HTTPclient

	// 3. Create Coinbase client
	client := coinbase.NewAPIClient(config)

	// 4. Create context
	ctx := context.Background()

	// 5. Use Coinbase client to modify current user
	// Beware of that this really change your name!
	fmt.Printf("\n\tName will be changed to '%s', you should change it back!\n\n", newName)
	modifiedUser, res, err := client.UsersApi.UpdateCurrentUser(ctx, &coinbase.UpdateCurrentUserOpts{
		UserModify: optional.NewInterface(
			coinbase.UserModify{
				Name: newName,
			},
		),
	})
	if err != nil {
		resBody, _ := ioutil.ReadAll(res.Body)
		log.Printf("response body: %s", string(resBody))
		log.Fatalf("failed to modify user: %s", err)
	}

	// 6. Show changed name
	log.Printf("current name is: %#v", modifiedUser.Data["name"])
}
