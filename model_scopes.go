/*
 * Coinbase API Go Library
 *
 * API client generated from [hmlkao/coinbase-openapi](http://gitlab.com/hmlkao/coinbase-openapi) repository.  This is pure output from [OpenAPI Generator](https://github.com/openapitools/openapi-generator), Coinbase API key authentication requires each request to be signed so it **wouldn't work with native http.Client**, follow [examples](https://gitlab.com/hmlkao/coinbase-go/-/tree/master/examples) how to use it. 
 *
 * API version: 2019-11-15
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package coinbase
// Scopes the model 'Scopes'
type Scopes string

// List of Scopes
const (
	ACCOUNTSREAD Scopes = "wallet:accounts:read"
	ACCOUNTSUPDATE Scopes = "wallet:accounts:update"
	ACCOUNTSCREATE Scopes = "wallet:accounts:create"
	ACCOUNTSDELETE Scopes = "wallet:accounts:delete"
	ADDRESSESREAD Scopes = "wallet:addresses:read"
	ADDRESSESCREATE Scopes = "wallet:addresses:create"
	BUYSREAD Scopes = "wallet:buys:read"
	BUYSCREATE Scopes = "wallet:buys:create"
	DEPOSITSREAD Scopes = "wallet:deposits:read"
	DEPOSITSCREATE Scopes = "wallet:deposits:create"
	NOTIFICATIONSREAD Scopes = "wallet:notifications:read"
	PAYMENT_METHODSREAD Scopes = "wallet:payment-methods:read"
	PAYMENT_METHODSDELETE Scopes = "wallet:payment-methods:delete"
	PAYMENT_METHODSLIMITS Scopes = "wallet:payment-methods:limits"
	SELLSREAD Scopes = "wallet:sells:read"
	SELLSCREATE Scopes = "wallet:sells:create"
	TRANSACTIONSREAD Scopes = "wallet:transactions:read"
	TRANSACTIONSSEND Scopes = "wallet:transactions:send"
	TRANSACTIONSREQUEST Scopes = "wallet:transactions:request"
	TRANSACTIONSTRANSFER Scopes = "wallet:transactions:transfer"
	USERREAD Scopes = "wallet:user:read"
	USERUPDATE Scopes = "wallet:user:update"
	USEREMAIL Scopes = "wallet:user:email"
	WITHDRAWALSREAD Scopes = "wallet:withdrawals:read"
	WITHDRAWALSCREATE Scopes = "wallet:withdrawals:create"
)
