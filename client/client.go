package client

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type transport struct {
	underlyingTransport http.RoundTripper
	apiKey              string
	apiSecret           string
}

func (t *transport) RoundTrip(req *http.Request) (*http.Response, error) {
	var bodyString string
	ts := strconv.FormatInt(time.Now().Unix(), 10)

	if req.Body != nil {
		// If you read body it closes the buffer and data are removed
		//   you have to reconstruct the body to be other handler able work with it
		// https://medium.com/@xoen/golang-read-from-an-io-readwriter-without-loosing-its-content-2c6911805361
		// https://stackoverflow.com/a/43021236/3887059
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			return nil, fmt.Errorf("Error reading body: %v", err)
		}
		bodyString = string(body)

		// Set a new body, which will simulate the same data we read
		req.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	}

	signKey := getSignKey(ts, req.Method, req.URL.Path, bodyString, t.apiSecret)

	req.Header.Add("CB-ACCESS-KEY", t.apiKey)
	req.Header.Add("CB-ACCESS-SIGN", signKey)
	req.Header.Add("CB-ACCESS-TIMESTAMP", ts)
	req.Header.Add("CB-VERSION", "2019-11-15")

	return t.underlyingTransport.RoundTrip(req)
}

// GetClient return HTTP client modified to use with Coinbase
func GetClient(apiKey, apiSecret string) http.Client {
	return http.Client{
		Transport: &transport{
			underlyingTransport: http.DefaultTransport,
			apiKey:              apiKey,
			apiSecret:           apiSecret,
		},
	}
}

func getSignKey(ts, method, requestPath, body, key string) string {
	mac := hmac.New(sha256.New, []byte(key))
	mac.Write([]byte(fmt.Sprintf("%s%s%s%s", ts, method, requestPath, body)))
	return hex.EncodeToString(mac.Sum(nil))
}
