/*
 * Coinbase API Go Library
 *
 * API client generated from [hmlkao/coinbase-openapi](http://gitlab.com/hmlkao/coinbase-openapi) repository.  This is pure output from [OpenAPI Generator](https://github.com/openapitools/openapi-generator), Coinbase API key authentication requires each request to be signed so it **wouldn't work with native http.Client**, follow [examples](https://gitlab.com/hmlkao/coinbase-go/-/tree/master/examples) how to use it. 
 *
 * API version: 2019-11-15
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package coinbase
// Notification struct for Notification
type Notification struct {
	// Resource ID
	Id string `json:"id,omitempty"`
	// Notification type
	Type string `json:"type,omitempty"`
	// Notification data. Related resource is available in `resource` key together with other available data. For `type: ping` the `data` will be an empty hash 
	Data map[string]interface{} `json:"data,omitempty"`
	User UserHash `json:"user,omitempty"`
}
