# UserModify

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | User’s public name | [optional] 
**TimeZone** | **string** | Time zone | [optional] 
**NativeCurrency** | **string** | Local currency used to display amounts converted from BTC | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


