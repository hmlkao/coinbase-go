# \AccountsApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Accounts**](AccountsApi.md#Accounts) | **Get** /accounts | List current user&#39;s accounts



## Accounts

> Accounts Accounts(ctx, )

List current user's accounts

Lists current user’s accounts to which the authentication method has access to.

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**Accounts**](Accounts.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

