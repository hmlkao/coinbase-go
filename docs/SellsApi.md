# \SellsApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Sell**](SellsApi.md#Sell) | **Get** /accounts/{accountId}/sells/{sellId} | Show a sell
[**SellCommit**](SellsApi.md#SellCommit) | **Post** /accounts/{accountId}/sells/{sellId}/commit | Commit a sell
[**SellPlaceOrder**](SellsApi.md#SellPlaceOrder) | **Post** /accounts/{accountId}/sells | Place sell order
[**Sells**](SellsApi.md#Sells) | **Get** /accounts/{accountId}/sells | List sells



## Sell

> Sell Sell(ctx, accountId, sellId)

Show a sell

Show an individual sell. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
**sellId** | **string**| Sell ID | 

### Return type

[**Sell**](Sell.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SellCommit

> Sell SellCommit(ctx, accountId, sellId)

Commit a sell

Completes a sell that is created in `commit: false` state.  If the exchange rate has changed since the sell was created, this call will fail with the error *“The exchange rate updated while you were waiting. The new total is shown below”*.  The sell’s total will also be updated. You can repeat the `/commit` call to accept the new values and commit the sell at the new rates. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
**sellId** | **string**| Sell ID | 

### Return type

[**Sell**](Sell.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SellPlaceOrder

> Sell SellPlaceOrder(ctx, accountId, optional)

Place sell order

Sells a user-defined amount of bitcoin, bitcoin cash, litecoin or ethereum.  There are two ways to define sell amounts–you can use either the `amount` or the `total` parameter: # When supplying amount, you’ll get the amount of bitcoin, bitcoin cash, litecoin or ethereum defined. With `amount` it’s recommended to use `BTC` or `ETH` as the `currency` value, but you can always specify a fiat currency and the amount will be converted to BTC or ETH respectively. # When supplying `total`, your payment method will be credited the total amount and you’ll get the amount in BTC or ETH after fees have been reduced from the subtotal. With `total` it’s recommended to use the currency of the payment method as the `currency` parameter, but you can always specify a different currency and it will be converted.  Given the price of digital currency depends on the time of the call and amount of the sell, it’s recommended to use the `commit: false` parameter to create an uncommitted sell to get a quote and then to commit that with [a separate request](https://developers.coinbase.com/api/v2#commit-a-sell).  If you need to query the sell price without locking in the sell, you can use `quote: true` option. This returns an unsaved sell and unlike `commit: false`, this sell can’t be completed. This option is useful when you need to show the detailed sell price quote for the user when they are filling a form or similar situation. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
 **optional** | ***SellPlaceOrderOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SellPlaceOrderOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **sellBody** | [**optional.Interface of SellBody**](SellBody.md)|  | 

### Return type

[**Sell**](Sell.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Sells

> SellList Sells(ctx, accountId)

List sells

Lists sells for an account. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 

### Return type

[**SellList**](SellList.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

