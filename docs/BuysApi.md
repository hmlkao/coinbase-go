# \BuysApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Buy**](BuysApi.md#Buy) | **Get** /accounts/{accountId}/buys/{buyId} | Show a buy
[**BuyCommit**](BuysApi.md#BuyCommit) | **Post** /accounts/{accountId}/buys/{buyId}/commit | Commit a buy
[**BuyPlaceOrder**](BuysApi.md#BuyPlaceOrder) | **Post** /accounts/{accountId}/buys | Place buy order
[**Buys**](BuysApi.md#Buys) | **Get** /accounts/{accountId}/buys | List buys



## Buy

> Buy Buy(ctx, accountId, buyId)

Show a buy

Show an individual buy. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
**buyId** | **string**| Buy ID | 

### Return type

[**Buy**](Buy.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## BuyCommit

> Buy BuyCommit(ctx, accountId, buyId)

Commit a buy

Completes a buy that is created in `commit: false` state.  If the exchange rate has changed since the buy was created, this call will fail with the error *“The exchange rate updated while you were waiting. The new total is shown below”*.  The buy’s total will also be updated. You can repeat the `/commit` call to accept the new values and start the buy at the new rates. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
**buyId** | **string**| Buy ID | 

### Return type

[**Buy**](Buy.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## BuyPlaceOrder

> Buy BuyPlaceOrder(ctx, accountId, optional)

Place buy order

Buys a user-defined amount of bitcoin, bitcoin cash, litecoin or ethereum.  There are two ways to define buy amounts–you can use either the `amount` or the `total` parameter: # When supplying `amount`, you’ll get the amount of bitcoin, bitcoin cash, litecoin or ethereum defined. With `amount` it’s recommended to use `BTC` or `ETH` as the currency value, but you can always specify a fiat currency and and the amount will be converted to BTC or ETH respectively. # When supplying `total`, your payment method will be debited the total amount and you’ll get the amount in BTC or ETH after fees have been reduced from the total. With `total` it’s recommended to use the currency of the payment method as the currency parameter, but you can always specify a different currency and it will be converted.  Given the price of digital currency depends on the time of the call and on the amount of purchase, it’s recommended to use the commit: false parameter to create an uncommitted buy to show the confirmation for the user or get the final quote, and commit that with [a separate request](https://developers.coinbase.com/api/v2#commit-a-buy).  If you need to query the buy price without locking in the buy, you can use `quote: true` option. This returns an unsaved buy and unlike `commit: false`, this buy can’t be completed. This option is useful when you need to show the detailed buy price quote for the user when they are filling a form or similar situation. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
 **optional** | ***BuyPlaceOrderOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a BuyPlaceOrderOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **buyBody** | [**optional.Interface of BuyBody**](BuyBody.md)|  | 

### Return type

[**Buy**](Buy.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Buys

> BuyList Buys(ctx, accountId)

List buys

Lists buys for an account. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 

### Return type

[**BuyList**](BuyList.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

