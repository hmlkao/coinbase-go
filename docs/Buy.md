# Buy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Resource ID | [optional] 
**Status** | **string** | Status of the buy. Currently available values: &#x60;created&#x60;, &#x60;completed&#x60;, &#x60;canceled&#x60;  | [optional] 
**PaymentMethod** | [**PaymentMethod**](PaymentMethod.md) |  | [optional] 
**Transaction** | [**Transaction**](Transaction.md) |  | [optional] 
**Amount** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**Total** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**Subtotal** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**Fee** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**CreatedAt** | **int32** |  | [optional] 
**UpdatedAt** | **int32** |  | [optional] 
**Resource** | **string** |  | [optional] 
**ResourcePath** | **string** |  | [optional] 
**Committed** | **bool** | Has this buy been committed? | [optional] 
**Instant** | **bool** | Was this buy executed instantly? | [optional] 
**PayoutAt** | **string** | When a buy isn’t executed instantly, it will receive a payout date for the time it will be executed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


