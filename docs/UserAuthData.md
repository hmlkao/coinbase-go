# UserAuthData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Method** | **string** |  | [optional] 
**Scopes** | [**[]Scopes**](Scopes.md) |  | [optional] 
**OauthMeta** | [**map[string]interface{}**](.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


