# SellBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | **string** | Sell amount without fees | 
**Total** | **string** | Sell amount with fees (alternative to &#x60;amount&#x60;) | [optional] 
**Currency** | **string** | Currency for the &#x60;amount&#x60; | 
**PaymentMethod** | **string** | The ID of the payment method that should be used for the sell. Payment methods can be listed using the &#x60;GET /payment-methods&#x60; API call | [optional] 
**AgreeBtcAmountVaries** | **bool** | Whether or not you would still like to sell if you have to wait for your money to arrive to lock in a price | [optional] 
**Commit** | **bool** | If set to &#x60;false&#x60;, this sell will not be immediately completed. Use the [commit](https://developers.coinbase.com/api/v2#commit-a-buy) call to complete it. Default value: &#x60;true&#x60;  | [optional] [default to true]
**Quote** | **bool** | If set to &#x60;true&#x60;, response will return an unsave sell for detailed price quote. Default value: &#x60;false&#x60;  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


