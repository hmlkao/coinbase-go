# PaymentMethod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Resource ID | [optional] 
**Type** | **string** | Payment method type | [optional] 
**Name** | **string** | Payment method type | [optional] 
**Currency** | **string** | Payment method’s native currency | [optional] 
**PrimarySell** | **bool** | Is primary selling method? | [optional] 
**AllowBuy** | **bool** | Is buying allowed with this method? | [optional] 
**AllowSell** | **bool** | Is selling allowed with this method? | [optional] 
**InstantBuy** | **bool** | Does this method allow for instant buys? | [optional] 
**InstantSell** | **bool** | Does this method allow for instant sells? | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Resource** | **string** |  | [optional] 
**ResourcePath** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


