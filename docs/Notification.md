# Notification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Resource ID | [optional] 
**Type** | **string** | Notification type | [optional] 
**Data** | [**map[string]interface{}**](.md) | Notification data. Related resource is available in &#x60;resource&#x60; key together with other available data. For &#x60;type: ping&#x60; the &#x60;data&#x60; will be an empty hash  | [optional] 
**User** | [**UserHash**](UserHash.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


