# \TransactionsApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Transaction**](TransactionsApi.md#Transaction) | **Get** /accounts/{accountId}/transactions/{transactionId} | Show a transaction
[**TransactionSend**](TransactionsApi.md#TransactionSend) | **Post** /accounts/{accountId}/transactions | Send money
[**Transactions**](TransactionsApi.md#Transactions) | **Get** /accounts/{accountId}/transactions | List transactions



## Transaction

> Transaction Transaction(ctx, accountId, transactionId)

Show a transaction

Show an individual transaction for an account. See transaction resource for more information.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
**transactionId** | **string**| Transaction ID | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## TransactionSend

> Transaction TransactionSend(ctx, accountId, optional)

Send money

Send funds to a bitcoin address, bitcoin cash address, litecoin address, ethereum address, or email address. No transaction fees are required for off blockchain bitcoin transactions.  It’s recommended to always supply a unique idem field for each transaction. This prevents you from sending the same transaction twice if there has been an unexpected network outage or other issue.  When used with OAuth2 authentication, this endpoint requires [two factor authentication](https://developers.coinbase.com/docs/wallet/coinbase-connect/two-factor-authentication). 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 
 **optional** | ***TransactionSendOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a TransactionSendOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **transaction** | [**optional.Interface of Transaction**](Transaction.md)|  | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Transactions

> TransactionList Transactions(ctx, accountId)

List transactions

Lists account’s transactions. See [transaction resource](https://developers.coinbase.com/api/v2#the-transaction-resource) for more information.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string**| Account ID | 

### Return type

[**TransactionList**](TransactionList.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

