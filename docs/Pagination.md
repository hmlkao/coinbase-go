# Pagination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Limit** | **int32** | Number of results per call. Accepted values: 0 - 100. Default 25 | [optional] 
**Order** | **string** | Result order | [optional] 
**StartingAfter** | **string** | A cursor for use in pagination. &#x60;starting_after&#x60; is an resource ID that defines your place in the list. | [optional] 
**EndingBefore** | **string** | A cursor for use in pagination. &#x60;ending_before&#x60; is an resource ID that defines your place in the list. | [optional] 
**PreviousUri** | **string** |  | [optional] 
**NextUri** | **string** |  | [optional] 
**PreviousEnditngBefore** | **string** |  | [optional] 
**PreviousStartingAfter** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


