# TransactionSend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | Type &#x60;send&#x60; is required when sending money | 
**To** | **string** | A bitcoin address, bitcoin cash address, litecoin address, ethereum address, or an email of the recipient | 
**Amount** | **string** | Amount to be sent | 
**Currency** | **string** | Currency for the &#x60;amount&#x60; | 
**Description** | **string** | Notes to be included in the email that the recipient receives | [optional] 
**SkipNotification** | **bool** | Don’t send notification emails for small amounts (e.g. tips) | [optional] 
**Fee** | **string** | Transaction fee in BTC/ETH/LTC if you would like to pay it. Fees can be added as a string, such as &#x60;0.0005&#x60; | [optional] 
**Idem** | **string** | **[Recommended]** A token to ensure [idempotence](http://en.wikipedia.org/wiki/Idempotence). If a previous transaction with the same &#x60;idem&#x60; parameter already exists for this sender, that previous transaction will be returned and a new one will not be created. Max length 100 characters  | [optional] 
**ToFinancialInstitution** | **bool** | Whether this send is to another financial institution or exchange. Required if this send is to an address and is valued at over USD$3000. | [optional] 
**FinancialInstitutionWebsite** | **string** | The website of the financial institution or exchange. Required if &#x60;to_financial_institution&#x60; is &#x60;true&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


