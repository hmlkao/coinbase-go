# AccountCurrency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Color** | **string** |  | [optional] 
**SortIndex** | **int32** |  | [optional] 
**Exponent** | **int32** |  | [optional] 
**Type** | **string** |  | [optional] 
**AddressRegex** | **string** |  | [optional] 
**AssetId** | **string** |  | [optional] 
**Slug** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


