# \TimeApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Time**](TimeApi.md#Time) | **Get** /time | Get current time



## Time

> Time Time(ctx, )

Get current time

Get the API server time.  **This endpoint doesn’t require authentication.** 

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**Time**](Time.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

