# \UsersApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CurrentUser**](UsersApi.md#CurrentUser) | **Get** /user | Show current user
[**UpdateCurrentUser**](UsersApi.md#UpdateCurrentUser) | **Put** /user | Update current user
[**User**](UsersApi.md#User) | **Get** /users/{userId} | Show a user
[**UserAuth**](UsersApi.md#UserAuth) | **Get** /user/auth | Show authorization information



## CurrentUser

> User CurrentUser(ctx, )

Show current user

Get current user’s public information. To get user’s email or private information, use permissions `wallet:user:email` and `wallet:user:read`. If current request has a `wallet:transactions:send` scope, then the response will contain a boolean `sends_disabled` field that indicates if the user’s send functionality has been disabled. 

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCurrentUser

> User UpdateCurrentUser(ctx, optional)

Update current user

Modify current user and their preferences. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
 **optional** | ***UpdateCurrentUserOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateCurrentUserOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userModify** | [**optional.Interface of UserModify**](UserModify.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## User

> User User(ctx, userId)

Show a user

Get any user’s public information with their ID.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**userId** | **string**| Resource ID | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UserAuth

> UserAuth UserAuth(ctx, )

Show authorization information

Get current user’s authorization information including granted scopes and send limits when using OAuth2 authentication.

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**UserAuth**](UserAuth.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

