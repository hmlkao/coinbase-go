# TransactionTransfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | Type &#x60;transfer&#x60; is required when transferring bitcoin or ethereum between accounts | 
**To** | **string** | ID of the receiving account | 
**Amount** | **string** | Amount to be transferred | 
**Currency** | **string** | Currency for the &#x60;amount&#x60; | 
**Description** | **string** | Notes to be included in the transfer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


