# Account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Resource ID | [optional] 
**Name** | **string** | User or system defined name | [optional] 
**Primary** | **bool** | Primary account | [optional] 
**Type** | **string** | Account’s type | [optional] 
**Currency** | [**AccountCurrency**](Account_currency.md) |  | [optional] 
**Balance** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**CreatedAt** | **string** |  | [optional] 
**UpdatedAt** | **string** |  | [optional] 
**Resource** | **string** |  | [optional] 
**ResourcePath** | **string** |  | [optional] 
**AllowDeposits** | **bool** |  | [optional] 
**AllowWithdrawals** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


