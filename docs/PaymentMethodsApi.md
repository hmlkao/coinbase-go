# \PaymentMethodsApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**PaymentMethod**](PaymentMethodsApi.md#PaymentMethod) | **Get** /payment-methods/{paymentMethodId} | Show a payment method
[**PaymentMethods**](PaymentMethodsApi.md#PaymentMethods) | **Get** /payment-methods | List payment methods



## PaymentMethod

> PaymentMethod PaymentMethod(ctx, paymentMethodId)

Show a payment method

Show current user’s payment method.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**paymentMethodId** | **string**| Payment method ID | 

### Return type

[**PaymentMethod**](PaymentMethod.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PaymentMethods

> PaymentMethodList PaymentMethods(ctx, )

List payment methods

Lists current user’s payment methods.

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**PaymentMethodList**](PaymentMethodList.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

