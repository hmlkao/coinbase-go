# TransactionData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Resource ID | [optional] 
**Type** | **string** | Transaction type | 
**Status** | **string** | Status | 
**Amount** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**NativeAmount** | [**MoneyHash**](MoneyHash.md) |  | [optional] 
**Description** | **string** | User defined description | [optional] 
**CreatedAt** | **int32** |  | [optional] 
**UpdatedAt** | **int32** |  | [optional] 
**Resource** | **string** |  | 
**ResourcePath** | **string** |  | [optional] 
**Details** | [**Details**](Details.md) |  | [optional] 
**Network** | [**Network**](Network.md) |  | [optional] 
**To** | [**Resource**](Resource.md) |  | [optional] 
**From** | [**Resource**](Resource.md) |  | [optional] 
**Address** | [**map[string]interface{}**](.md) | Associated bitcoin, bitcoin cash, litecoin or ethereum address for received payment | [optional] 
**Application** | **string** | Associated OAuth2 application | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


