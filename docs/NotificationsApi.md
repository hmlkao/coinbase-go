# \NotificationsApi

All URIs are relative to *https://api.coinbase.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Notification**](NotificationsApi.md#Notification) | **Get** /notifications/{notificationId} | Show a notification
[**Notifications**](NotificationsApi.md#Notifications) | **Get** /notifications | List notifications



## Notification

> Notification Notification(ctx, notificationId)

Show a notification

Show a notification for which the current user was a subsciber. 

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**notificationId** | **string**| Resource ID | 

### Return type

[**Notification**](Notification.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## Notifications

> NotificationList Notifications(ctx, )

List notifications

Lists notifications where the current user was the subscriber (owner of the API key or OAuth application). 

### Required Parameters

This endpoint does not need any parameter.

### Return type

[**NotificationList**](NotificationList.md)

### Authorization

[oAuth2](../README.md#oAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

